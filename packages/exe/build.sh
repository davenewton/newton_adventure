#!/usr/bin/env bash
echo "Create target directory"
mkdir -p target
cd target

echo "Download bundable openjdk"
wget -c https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.10%2B7/OpenJDK17U-jre_x64_windows_hotspot_17.0.10_7.zip
unzip OpenJDK17U-jre_x64_windows_hotspot_17.0.10_7.zip
mv jdk-17.0.10+7-jre jre

echo "Copy jar"
cp ../../../game/lwjgl/target/newton-adventure.jar ./newton_adventure.jar

echo "Create exe"
cp ../newton_adventure.nsi ./
cp ../icon.ico ./
makensis newton_adventure.nsi
