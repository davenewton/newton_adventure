#!/bin/sh
echo "Create target directory"
mkdir -p target
cp -R Newton_Adventure.AppDir target/Newton_Adventure.AppDir
cd target

echo "Download bundable opendjk"
wget -c https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.10%2B7/OpenJDK17U-jdk_x64_linux_hotspot_17.0.10_7.tar.gz
tar xf OpenJDK17U-jdk_x64_linux_hotspot_17.0.10_7.tar.gz

echo "Copy Newton Adventure jar"
cp ../../../game/lwjgl/target/newton-adventure.jar Newton_Adventure.AppDir/

echo "Build custom jdk, if something fail, try to remove --ignore-missing-deps options"
NEWTON_ADVENTURE_DEPS=`./jdk-17.0.10+7/bin/jdeps --ignore-missing-deps --print-module-deps Newton_Adventure.AppDir/newton-adventure.jar`
./jdk-17.0.10+7/bin/jlink --no-header-files --no-man-pages --compress=2 --strip-debug --add-modules $NEWTON_ADVENTURE_DEPS --output Newton_Adventure.AppDir/usr

wget -c https://github.com/AppImage/AppImageKit/releases/download/13/appimagetool-x86_64.AppImage
chmod +x appimagetool-x86_64.AppImage
ARCH=x86_64 ./appimagetool-x86_64.AppImage Newton_Adventure.AppDir/
