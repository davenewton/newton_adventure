#!/usr/bin/env sh
GITLAB_HOST="gitlab.com"
GITLAB_USER="davenewton"
NEWTON_ADVENTURE_VERSION="${NEWTON_ADVENTURE_VERSION}"

curl -u "${GITLAB_USER}:${GITLAB_TOKEN}" --upload-file packages/appimage/target/Newton_Adventure-x86_64.AppImage https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fnewton_adventure/packages/generic/newton_adventure/${NEWTON_ADVENTURE_VERSION}/Newton_Adventure-x86_64.AppImage | tee /dev/null
curl -u "${GITLAB_USER}:${GITLAB_TOKEN}" --upload-file packages/exe/target/newton_adventure.exe https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fnewton_adventure/packages/generic/newton_adventure/${NEWTON_ADVENTURE_VERSION}/newton_adventure.exe | tee /dev/null
curl -u "${GITLAB_USER}:${GITLAB_TOKEN}" --upload-file game/lwjgl/target/newton-adventure.jar https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fnewton_adventure/packages/generic/newton_adventure/${NEWTON_ADVENTURE_VERSION}/newton-adventure.jar | tee /dev/null

curl --request POST "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fnewton_adventure/releases" \
     --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
     --data @- <<REQUEST_BODY
{
  "name": "${NEWTON_ADVENTURE_VERSION}",
  "tag_name": "${NEWTON_ADVENTURE_VERSION}",
  "ref": "master",
  "description": "Release ${NEWTON_ADVENTURE_VERSION}",
  "assets": {
    "links": [
      {
        "name": "appimage",
        "url": "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fnewton_adventure/packages/generic/newton_adventure/${NEWTON_ADVENTURE_VERSION}/Newton_Adventure-x86_64.AppImage",
        "filepath": "/binaries/Newton_Adventure-x86_64.AppImage",
        "link_type": "package"
      },
      {
        "name": "exe",
        "url": "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fnewton_adventure/packages/generic/newton_adventure/${NEWTON_ADVENTURE_VERSION}/newton_adventure.exe",
        "filepath": "/binaries/newton_adventure.exe",
        "link_type": "package"
      },
      {
        "name": "jar",
        "url": "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Fnewton_adventure/packages/generic/newton_adventure/${NEWTON_ADVENTURE_VERSION}/newton-adventure.jar",
        "filepath": "/binaries/newton-adventure.jar",
        "link_type": "package"
      }
    ]
  }
}

