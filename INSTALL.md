Requirements
============

Java JDK LTS, [openjdk](http://openjdk.java.net/)
[Maven](https://maven.apache.org/) 3+,
[bash](https://www.gnu.org/software/bash/) 4+
[NSIS](https://nsis.sourceforge.io/)

Debian users could go to the source folder and run the install_debian_prerequisites.sh script using bash:

        bash install_debian_prerequisites.sh

Build
=====

Clone the repository with submodules then go to the source folder
and run the build.sh script using bash:

	bash build.sh

Run 
===

Go to the source folder and run the play.sh script using bash:

        bash play.sh

Create packages
===============

Go to the source folder and run the package.sh script using bash:

bash package.sh

Develop, debug
==============

Newton Adventure is maven based project, open it with Netbeans or Eclipse.

Contribute
==========

Please contact me by mail: devnewton at bci.im

