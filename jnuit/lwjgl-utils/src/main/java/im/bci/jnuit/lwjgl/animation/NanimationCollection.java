package im.bci.jnuit.lwjgl.animation;

import im.bci.jnuit.animation.IAnimationCollection;
import im.bci.jnuit.lwjgl.LwjglHelper;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.lwjgl.opengl.GL11;

public class NanimationCollection implements IAnimationCollection {

    LinkedHashMap<String/*animation name*/, Nanimation> animations;
    private final Map<String, NanimationImage> images;
    
    public NanimationCollection() {
        animations = new LinkedHashMap<String/*animation name*/, Nanimation>();
        images = new HashMap<String, NanimationImage>();
    }

    public void addAnimation(Nanimation animation) {
        animations.put(animation.getName(), animation);
    }

    @Override
    public Nanimation getFirst() {
        return animations.values().iterator().next();
    }

    @Override
    public Nanimation getAnimationByName(String name) {
        Nanimation nanimation = animations.get(name);
        if(null != nanimation) {
            return nanimation;
        } else {
            throw new RuntimeException("Unknown animation " + name);
        }        
    }

    public Map<String, NanimationImage> getImages() {
        return images;
    }
}
